(function POC(DATA) {
    const elements = document.querySelectorAll('.element');

    $(document).tooltip({
        items: ".element",
        track: true,
        content: function() {
            const { facts } = DATA[this.id];
            const list = facts.map((f) => `<li><span class="label">${f.label}: </span><span class="value">${f.value}</span></li>`);

            return `
                <ul>
                    ${list.join('')}
                </ul>
            `;
        }
    });
})(DATA);